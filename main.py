from windowApp import WindowApp

from PySide2.QtWidgets import QApplication
import sys


def main():
    """
        create main window with PySide2
    """
    app = QApplication(sys.argv)
    window = WindowApp(sys.argv)
    window.show()

    app.exec_()
    sys.exit(0)


if __name__ == '__main__':
    main()
