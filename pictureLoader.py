from PySide2.QtGui import QPixmap
import glob


class PictureLoader:
    def __init__(self, picture_path):
        self.picture = QPixmap(picture_path)

        self.folder_path, self.picture_name = self.parse_path_to_folder(picture_path)
        self.all_pictures_in_folder = glob.glob(self.folder_path + "\\*")
        self.index_of_current_picture = self.all_pictures_in_folder.index(picture_path)
        print(self.index_of_current_picture)

    def get_current_picture(self):
        return self.picture

    def get_picture_folder_path(self):
        return self.folder_path

    def get_picture_name(self):
        return self.picture_name

    def set_index(self, move_index):
        if 0 <= self.index_of_current_picture + move_index < len(self.all_pictures_in_folder):
            self.index_of_current_picture = self.index_of_current_picture + move_index

    def set_picture(self):
        self.picture = QPixmap(self.all_pictures_in_folder[self.index_of_current_picture])

    @staticmethod
    def parse_path_to_folder(picture_path: str) -> tuple[str, str]:
        split_path = picture_path.split('\\')
        return '\\'.join(split_path[:-1]), split_path[-1]
