from pictureLoader import PictureLoader

from PySide2.QtWidgets import QApplication, QWidget, QLabel, QHBoxLayout
from PySide2.QtCore import Qt
import sys


class WindowApp(QWidget):
    WIDTH = 1280
    HEIGHT = 720

    def __init__(self, argv):
        super().__init__()
        self.argv = argv
        self.setWindowTitle("Viewer")
        self.setGeometry(150, 150, self.WIDTH, self.HEIGHT)

        self.go_left_label = QLabel()
        self.go_left_label.setStyleSheet("QLabel::hover"
                                         "{"
                                         "background-color : lightgreen;"
                                         "}")
        self.go_left_label.mousePressEvent = self.move_index_left

        self.picture_label = QLabel()

        self.go_right_label = QLabel()
        self.go_right_label.setStyleSheet("QLabel::hover"
                                          "{"
                                          "background-color : lightgreen;"
                                          "}")
        self.go_right_label.mousePressEvent = self.move_index_right

        new_string = ', '.join(self.argv[0].split('/'))
        if len(self.argv) > 1:
            self.picture_loader = PictureLoader(self.argv[1])
            self.picture_label.setPixmap(self.picture_loader.get_current_picture())
            self.setWindowTitle(f"Viewer - {self.picture_loader.get_picture_name()}")
        else:
            self.picture_label.setText(new_string)
        self.layout = QHBoxLayout()

        self.layout.addWidget(self.go_left_label)
        self.layout.addWidget(self.picture_label)
        self.layout.addWidget(self.go_right_label)

        self.layout.setAlignment(Qt.AlignHCenter)
        self.setLayout(self.layout)

    def move_index_left(self, event):
        self.picture_loader.set_index(-1)
        self.picture_loader.set_picture()
        self.picture_label.setPixmap(self.picture_loader.get_current_picture())
        self.setWindowTitle(f"Viewer - {self.picture_loader.get_picture_name()}")

    def move_index_right(self, event):
        self.picture_loader.set_index(1)
        self.picture_loader.set_picture()
        self.picture_label.setPixmap(self.picture_loader.get_current_picture())
        self.setWindowTitle(f"Viewer - {self.picture_loader.get_picture_name()}")


if __name__ == "__main__":
    App = QApplication(sys.argv)
    window = WindowApp()
    window.show()

    App.exec_()
    sys.exit(0)
